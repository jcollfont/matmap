function tsinfo_struct = create_tsinfo_struct()
%% Create_Struct.m
%% 12/22/97:	Sri
%% This function interactively builds up the members of the structure, TSInfo
%% TSInfo contains the following 10 fields, 
%% 	numleads
%% 	numframes 
%%      format
%%      units
%%    	numbadleads
%%	badleadlistarray
%%      pakfile
%%      geomfile
%%      label
%%      potval
%% Of the above 10 fields, not all fields are necessary to build a valid data 
%% file, default values are taken for all those fields for which the user has
%% no information to provide
%% Usage: Mytsinfo = create_struct


fprintf(1, '\n\n CREATE TSINFO STRUCTURE \n');
fprintf(1, '\n\n This function interactively creates the fields of the structure\n')
fprintf(1, 'TSInfo, A default information is added where no information is provided\n')
fprintf(1, 'The structure tsinfo has 10 fields\n')
fprintf(1, 'Fields marked REQUIRED has to be provided by the user \n\n');

tsinfo_struct.numleads = input('(REQUIRED) Number of leads');
tsinfo_struct.numframes = input('(REQUIRED) Number of frames');
tsinfo_struct.format = input('Format (default 0)');
if (length(tsinfo_struct.format) == 0) 
	tsinfo_struct.format = 0;
end
tsinfo_struct.units = input('Units (default 2)');
if (length(tsinfo_struct.units) == 0) 
	tsinfo_struct.units = 2;
end
tsinfo_struct.numbadleads = input('Number of bad leads (default 0)');
if (length(tsinfo_struct.numbadleads) == 0) 
	tsinfo_struct.numbadleads = 0;
	tsinfo_struct.badleadlistarray = [];
end

if (tsinfo_struct.numbadleads ~= 0)
	if(tsinfo_struct.numbadleads > tsinfo_struct.numleads)
		error(' Number of bad leads greater than number of leads');
	end

	for i=1:tsinfo_struct.numbadleads
		tsinfo_struct.badleadlistarray(i) = input(['Array Number of badlead number   ' num2str(i) '----->' ]); 
	end
end

tsinfo_struct.pakfile = input('PAK File Name', 's');
tsinfo_struct.geomfile = input('Geometry File Name', 's');
tsinfo_struct.label = input('Label for the time series', 's');
if (length(tsinfo_struct.label) == 0) 
	tsinfo_struct.label = 'Data file structure using create_struct';
end
tsinfo_struct.potval = [];

fprintf(1, 'Now the structure is ready for use, The potentials should be\n')
fprintf(1, 'added to the field \"potval\". The potentials should be an array\n')
fprintf(1, 'of size %d nodes by %d timeframes\n', tsinfo_struct.numleads, tsinfo_struct.numframes)
