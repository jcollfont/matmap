/*
 * File: read_file.c
 * Author: Quan Ni [quan@cvrti.utah.edu]
 * Date: Tue Dec 30 16:16:45 1997
 * 
 * Description: 
 * $Id: read_file.c,v 1.1.1.1 2001/02/11 02:59:57 macleod Exp $ 
 */

#include <stdio.h> 
#include "mex.h"
#include "data_mat.c"

void 
mexFunction(int nlhs, mxArray *plhs[],
	    int nrhs, const mxArray *prhs[])
{
  int buflen, status;
  char *fName;
  double *pr;
  int numStructDims = 2;
  int structDims[2] = {1,1};    /* size of structure */
  int numFields = 3;
  const char *fieldNames[] = {
    "text",                   /* main file header text */
    "expid",                  /* experiment ID */
    "numts"                   /* number of time series in the file */
  };
  mxArray *struct_array_ptr, *array_ptr;
  FInfo *fileInfo;

  /* put input file name in fName */
  buflen = mxGetM(prhs[0])*mxGetN(prhs[0]) + 1;
  fName = mxCalloc(buflen, sizeof(char));
  status = mxGetString(prhs[0], fName, buflen);
  if (status == 0)
    mexPrintf("Input file: %s \n", fName);
  else
    mexErrMsgTxt("Could not get input file name.");

  /* the output structure */
  struct_array_ptr = mxCreateStructArray(numStructDims,structDims,
					 numFields,fieldNames);
  if (struct_array_ptr == NULL)
    mexErrMsgTxt("Could not create struct array. \n");
  else
    mxSetName(struct_array_ptr, "FileInfo");

  /* get file info by calling the C function */
  fileInfo = GetFileInfo(fName);
  if (fileInfo == NULL) {
    printf("Nothing returned. \n");
    return;
  }

  /*
  mexPrintf("Text: %s\n", fileInfo->text);
  mexPrintf("Experiment ID: %s\n", fileInfo->expid);
  mexPrintf("Number of Time Series: %d\n", fileInfo->numts);
  */

  /* pass fileinfo to the output structure */
  if (strlen(fileInfo->text) > 0) {
    array_ptr = mxCreateString(fileInfo->text);
    mxSetField(struct_array_ptr, 0, fieldNames[0], array_ptr);
  }
  if (strlen(fileInfo->expid) > 0) {
    array_ptr = mxCreateString(fileInfo->expid);
    mxSetField(struct_array_ptr, 0, fieldNames[1], array_ptr);
  }
  
  array_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
  pr = mxGetPr(array_ptr);
  *pr = fileInfo->numts;
  mxSetField(struct_array_ptr, 0, fieldNames[2], array_ptr);
  
  nlhs = 1; 
  plhs[0] = struct_array_ptr; 
  free(fileInfo); 
}


