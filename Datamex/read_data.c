/*
 * File: read_data.c
 * Author: Srinidhi Kadagattur and Quan Ni 
 * Date: Tue Dec 30 16:14:18 1997
 * 		
 * Description: 
 * $Id: read_data.c,v 1.1.1.1 2001/02/11 02:59:58 macleod Exp $ 
 */

#include <stdio.h>
#include <math.h>
#include "mex.h"
#include "data_mat.c"

#define rows 1
#define cols 1

void
mexFunction(int nlhs,mxArray *plhs[],
	    int nrhs,const mxArray *prhs[])
{
  TSinfo *tsInfo;
  FInfo *fileInfo;
  int numTS, i, j, ndim=2, dims[2] = {rows, cols};
  char *fName;
  double  tmp, *pr;
  int num_fields = 10, buflen, status, timeseries;
  int begTS, endTS, numrows, numcols, *timeseriesarray;
  mxArray *struct_array_ptr, *array_ptr;
  const char *field_names[] = {
    "numleads",		/* Number of nodes */
    "numframes",	/* Number for frames in timeseries */
    "format",		/* Format */
    "units",		
    "numbadleads",	/* Total Number of badleads */
    "badleadlistarray",	/* list of badleads, 0 if no badleads */
    "pakfile",		/* Name of Pak File, not supported */
    "geomfile",		/* Name of the geometry File */
    "label",		/* Label for the time Series */
    "potval"		/* Potvals, 2-D array of numleads x numframes */
    /* Not included so far is the fiducial information */
  };

  /* Check for proper number of arguments */
  if (nrhs != 1 && nrhs != 2 && nrhs != 3) 
    mexErrMsgTxt("Only one, two or three input arguments allowed.\nUsage: tsinfo = read_data('Filename');\n tsinfo = read_data(filename, [begTS:endTS])\n tsinfo = read_data(fileName, begTS, endTS) \n");
  
  /* Obtain in the .data file from which data is to be read in */
  /*-----------------------------------------------------------*/
  /* Find out how long the input string array is. */
   buflen = (mxGetM(prhs[0]) * mxGetN(prhs[0])) + 1;

 /* Allocate enough memory to hold the converted string. */ 
   fName = mxCalloc(buflen, sizeof(char));
   if (fName == NULL)
     mexErrMsgTxt("Not enough heap space to hold converted string.");
   
 /* Copy the string data from prhs[0] and place it into buf. */ 
   status = mxGetString(prhs[0], fName, buflen); 
   if (status == 0)
     mexPrintf("Input file: %s \n", fName);
   else
     mexErrMsgTxt("Could not get input file name.");
   
   fileInfo = GetFileInfo(fName);
   
   /*
  mexPrintf("Text: %s\n", fileInfo->text);

    mexPrintf("Experiment ID: %s\n", fileInfo->expid);
   mexPrintf("Number of Time Series: %d\n", fileInfo->numts);
   */

   if (nrhs == 3) {
     begTS = mxGetScalar(prhs[1]);
     endTS = mxGetScalar(prhs[2]);
     numTS = endTS - begTS + 1;
     timeseriesarray = (int *)calloc(numTS, sizeof(int));
     for(i=0; i<numTS; i++)
       {
	 timeseriesarray[i] = begTS+i;
       }

   }
   else if(nrhs == 2) {
     numrows = mxGetM(prhs[1]);
     numcols = mxGetN(prhs[1]);
     if (numrows != 1 && numcols != 1) {
       mexErrMsgTxt("The second argument must be a scalar or a vector, \n Matrix is not supported here \n");
     }

     /*     numTS = max(numrows, numcols);*/
     if (numrows > numcols)
       numTS = numrows;
     else
       numTS = numcols;

     if(numTS == 1)
       mexPrintf("You have requested just one timeseries\n");

     pr = mxGetPr(prhs[1]);
     timeseriesarray = (int *)calloc(numTS, sizeof(int));
     for(i=0; i<numTS; i++)
       {
	 timeseriesarray[i] = (int)pr[i];
       }

   }
   else {
     begTS = 1;
     endTS = fileInfo->numts;
     numTS = endTS - begTS + 1;
     timeseriesarray = (int *)calloc(numTS, sizeof(int));
     for(i=0; i<numTS; i++)
       {
	 timeseriesarray[i] = begTS+i;
       }

   }

   /* Create an array of structs. */

   dims[1] = numTS;
   struct_array_ptr = mxCreateStructArray(ndim, dims, 
					  num_fields, field_names);

   if (struct_array_ptr == NULL)
     mexErrMsgTxt("Could not create struct array.\n");
   else
     mxSetName(struct_array_ptr, "TSInfo");

   tsInfo = GetTSinfo(fName, timeseriesarray, numTS);
   if (tsInfo == NULL) {
     mexPrintf("Nothing returned. \n");
     return;
   }
   
   
   for(timeseries = 0; timeseries<numTS; timeseries++)
     {
       array_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
       pr = mxGetPr(array_ptr);
       tmp = (double)tsInfo[timeseries].numleads; 
       memcpy(pr, &tmp, sizeof(double));
       mxSetField(struct_array_ptr, timeseries, field_names[0], array_ptr);
       
       array_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
       pr = mxGetPr(array_ptr);
       tmp = (double)tsInfo[timeseries].numframes;
       memcpy(pr,  &tmp, sizeof(double));
       mxSetField(struct_array_ptr, timeseries, field_names[1], array_ptr);
       
       
       array_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
       pr = mxGetPr(array_ptr);
       tmp = (double)tsInfo[timeseries].format;
       memcpy(pr,  &tmp, sizeof(double));
       mxSetField(struct_array_ptr, timeseries , field_names[2], array_ptr);
       
       
       array_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
       pr = mxGetPr(array_ptr);
       tmp = (double)tsInfo[timeseries].units;
       memcpy(pr,  &tmp, sizeof(double));
       mxSetField(struct_array_ptr, timeseries, field_names[3], array_ptr);
       
       array_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
       pr = mxGetPr(array_ptr);
       tmp = (double)tsInfo[timeseries].numbadleads;
       memcpy(pr,  &tmp, sizeof(double));
       mxSetField(struct_array_ptr, timeseries, field_names[4], array_ptr);
       
       if(tmp != (double)0.0)
	 {
	   array_ptr = mxCreateDoubleMatrix(tsInfo[timeseries].numbadleads,1,mxREAL);
	   pr = mxGetPr(array_ptr);
	   
	   memcpy(pr,  tsInfo[timeseries].badleadlist, tsInfo[timeseries].numbadleads*sizeof(double));
	   mxSetField(struct_array_ptr, timeseries, field_names[5], array_ptr);
	 }
       
   
       /* Copy PAK file name into the mxArray */
       if (strlen(tsInfo[timeseries].pakfile) > 0) {
	 array_ptr = mxCreateString(tsInfo[timeseries].pakfile);
	 mxSetField(struct_array_ptr, timeseries, field_names[6], array_ptr);
       }
       
       /* Copy Geometry file name into the mxArray one character at a time. */

       if (strlen(tsInfo[timeseries].geomfile) > 0) {
	 array_ptr = mxCreateString(tsInfo[timeseries].geomfile);
	 mxSetField(struct_array_ptr, timeseries, field_names[7], array_ptr);
       }
       
       /* Copy Label into the mxArray one character at a time. */

       if (strlen(tsInfo[timeseries].label) > 0) {
	 array_ptr = mxCreateString(tsInfo[timeseries].label);
	 mxSetField(struct_array_ptr, timeseries, field_names[8], array_ptr);
       }
       
       /* Copy Potential Values into array_ptr for reading into MATLAB */
       array_ptr = mxCreateDoubleMatrix(tsInfo[timeseries].numleads,
					tsInfo[timeseries].numframes,mxREAL); 

       pr = mxGetPr(array_ptr);
       for(i=0; i<tsInfo[timeseries].numframes; i++) 
	 { 
	   for(j=0; j<tsInfo[timeseries].numleads; j++)
	     {
	       pr[j+i*tsInfo[timeseries].numleads] = 
		 tsInfo[timeseries].potvals[i][j];
	     }
	 }
       
       mxSetField(struct_array_ptr, timeseries, field_names[9], array_ptr);
      

       /* Print list of fields and respective values  */
       mexPrintf("Number of Leads: %d\n", tsInfo[timeseries].numleads);
       mexPrintf("Number of Frames: %d\n",tsInfo[timeseries].numframes);
       
       /*
       mexPrintf("Format: %d\n", tsInfo[timeseries].format);
       mexPrintf("Unit: %d\n", tsInfo[timeseries].units);
       mexPrintf("Number of Bad Leads: %d\n", tsInfo[timeseries].numbadleads);
       mexPrintf("pakfile: %s\n", tsInfo[timeseries].pakfile);
       mexPrintf("geometry file: %s\n", tsInfo[timeseries].geomfile);
       mexPrintf("label: %s\n\n\n", tsInfo[timeseries].label);
       */
     }    

   /* Return tsInfo */
   nlhs = 1;
   plhs[0] = struct_array_ptr;
   
   free(tsInfo); 
   free(fileInfo);
   free(timeseriesarray);
}



