/*
 * File: write_data.c
 * Author: Quan Ni [quan@vissgi.cvrti.utah.edu] and	
 *	Srinidhi Kadagattur <kgsnidhi@cdsp.neu.edu>
 * Date: Tue Dec 30 16:17:13 1997
 * 
 * Description: 
 * $Id: write_data.c,v 1.1.1.1 2001/02/11 02:59:58 macleod Exp $ 
 */

#include <stdio.h>
#include "mex.h"
#include "data_mat.c"

#define rows 1
#define cols 1

void
mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray 
	    *prhs[])
{

  TSinfo *tsInfo;
  FInfo *fileInfo;
  int numTS, i;
  char *buf,charbuf[80];
  double  *pr;
  int num_fields = 10, buflen, status, timeseries;
  int field_num, framenum, leadnum, potind;
  long *badleadsarray;

  mxArray *array_ptr;

  const char *field;
  const char*f_field_names[] = {
    "text", 
    "expid",
    "numts"
  };
  const char *field_names[] = {
    "numleads",		/* Number of nodes */
    "numframes",	/* Number for frames in timeseries */
    "format",		/* Format */
    "units",		
    "numbadleads",	/* Total Number of badleads */
    "badleadlistarray",	/* list of badleads, 0 if no badleads */
    "pakfile",		/* Name of Pak File, not supported */
    "geomfile",		/* Name of the geometry File */
    "label",		/* Label for the time Series */
    "potval"		/* Potvals, 2-D array of numleads x numframes */
    /* Non included so far is the fiducial information */
  };
  /*BEGIN*/

  if(nrhs != 3)
    {
      mexErrMsgTxt("Usage: write_data(output_data_filename, "
		   "fileInfo, tsInfo)\n");
    }
  
  /* Obtain in the .data file from which data is to be read in */
  /*-----------------------------------------------------------*/
  /* Find out how long the input string array is. */
  buflen = (mxGetM(prhs[0]) * mxGetN(prhs[0])) + 1;

  /* Allocate enough memory to hold the converted string. */ 
  buf = mxCalloc(buflen, sizeof(char));
  if (buf == NULL)
    mexErrMsgTxt("Not enough heap space to hold converted string.");
  
  /* Copy the string data from prhs[0] and place it into buf. */ 
  status = mxGetString(prhs[0], buf, buflen); 
  if (status == 0)
    mexPrintf("The converted string is \n%s.\n", buf);
  else
    mexErrMsgTxt("Could not convert string data.");
   
   /* The .data output filename is in "buf" */
   /*-----------------------------------------------------------*/

  if (mxIsStruct(prhs[1]))  { 
    /* Get number of fields in the input structure mxArray. */ 
    num_fields = mxGetNumberOfFields(prhs[1]);
    /*
    mexPrintf("fileInfo structure has %d fields.\n", num_fields);
    */
  }
  else
    mexErrMsgTxt("You must specify a structure"); 
   
  if (num_fields == 0)
    mexWarnMsgTxt("This structure has no fields.");
  else  {
     /* Get the first field name, then the second, and so on. */ 
    for (field_num=0; field_num<num_fields; field_num++) {
      field = mxGetFieldNameByNumber(prhs[1], field_num);
      /*	
	mexPrintf("%s\n", field);
	*/
    }
  }
  
  fileInfo = (FInfo *)calloc(1, sizeof(FInfo));
  array_ptr = mxGetField(prhs[1],0,f_field_names[0]);
  if(array_ptr == NULL)
    {
      buflen=1;
      strcpy(fileInfo->text, "\0");
    }
  else
    {
      buflen = (mxGetM(array_ptr) * mxGetN(array_ptr)) + 1; 
      status = mxGetString(array_ptr, charbuf, buflen);  
      if (status == 0)
	mexPrintf("The converted string is \n%s.\n", charbuf);
      else
	mexErrMsgTxt("Could not convert string data.");
      
      strcpy(fileInfo->text, charbuf);
    }
  
  array_ptr = mxGetField(prhs[1],0,f_field_names[1]);
  if(array_ptr == NULL)
     {
       buflen=1;
       strcpy(fileInfo->expid, "\0");
     }
   else
     {
       buflen = (mxGetM(array_ptr) * mxGetN(array_ptr)) + 1; 
       status = mxGetString(array_ptr, charbuf, buflen);  
       if (status == 0)
	 mexPrintf("The converted string is \n%s.\n", charbuf);
       else
	 mexErrMsgTxt("Could not convert string data.");
       
       strcpy(fileInfo->expid, charbuf);
     }
   array_ptr = mxGetField(prhs[1],0,f_field_names[2]);
   pr = mxGetPr(array_ptr);
   fileInfo->numts = (long)*pr;
   
   if (mxIsStruct(prhs[2]))  { 
     /* Get number of fields in the input structure mxArray. */ 
       num_fields = mxGetNumberOfFields(prhs[2]);
       /*
       mexPrintf("ts structure has %d fields.\n", num_fields);
       */
   }
   else
     mexErrMsgTxt("You must specify a structure"); 
   
   if (num_fields == 0)
     mexWarnMsgTxt("This structure has no fields.");
   else  {
     /* Get the first field name, then the second, and so on. */ 
     for (field_num=0; field_num<num_fields; field_num++) {
       field = mxGetFieldNameByNumber(prhs[2], field_num);
       /*
	 mexPrintf("%s\n", field);
	 */
     }
   }

   /*
   mexPrintf("Rows = %d\tCols = %d\n", mxGetM(prhs[2]), mxGetN(prhs[2]));
   */

   numTS = mxGetN(prhs[2]);		/* Number of Time Series */

   /*
   mexPrintf("Number of Time Series: %d\n", numTS);
   */

   /* Allocate space for all the timeseries */
   tsInfo = (TSinfo *)calloc(numTS, sizeof(TSinfo));


   for(timeseries = 0; timeseries<numTS; timeseries++)
     {

       array_ptr = mxGetField(prhs[2],timeseries,field_names[0]);
       pr = mxGetPr(array_ptr);
       tsInfo[timeseries].numleads = (long)*pr;
       /*
       mexPrintf("numleads = %d\n", tsInfo[timeseries].numleads);
       */

       array_ptr = mxGetField(prhs[2],timeseries,field_names[1]);
       pr = mxGetPr(array_ptr);
       tsInfo[timeseries].numframes = (long)*pr;
       /*
       mexPrintf("numframes = %d\n", tsInfo[timeseries].numframes);
       */

       array_ptr = mxGetField(prhs[2],timeseries,field_names[2]);
       pr = mxGetPr(array_ptr);
       tsInfo[timeseries].format = (long)*pr;
       /*
       mexPrintf("format = %d\n", tsInfo[timeseries].format);
       */
       array_ptr = mxGetField(prhs[2],timeseries,field_names[3]);
       pr = mxGetPr(array_ptr);
       tsInfo[timeseries].units = (long)*pr;
       /*       
       mexPrintf("Units = %d\n", tsInfo[timeseries].units);
       */
       array_ptr = mxGetField(prhs[2],timeseries,field_names[4]);
       pr = mxGetPr(array_ptr);
       tsInfo[timeseries].numbadleads = (long)*pr;
       /*
        mexPrintf("numbadleads = %d\n", tsInfo[timeseries].numbadleads);
	*/
       if(tsInfo[timeseries].numbadleads != 0)
	 {
	   array_ptr = mxGetField(prhs[2],timeseries,field_names[5]);
	   pr = mxGetPr(array_ptr);
	   badleadsarray = (long *)calloc(tsInfo[timeseries].numbadleads, sizeof(long));
	   for(i=0; i<tsInfo[timeseries].numbadleads; i++)
	     {
	       badleadsarray[i] = (long)pr[i];
	     }
	   tsInfo[timeseries].badleadlist = badleadsarray;
	 }
       else
	 {
	   tsInfo[timeseries].badleadlist = NULL;
	 }
       
       
       /* Copy the "pakfile" string from prhs[2] and place it into charbuf. */ 
       array_ptr = mxGetField(prhs[2],timeseries,field_names[6]);

       if(array_ptr == NULL)
	 {
	   buflen=1;
	   strcpy(tsInfo[timeseries].pakfile, "\0");
	 }
       else
	 {
	   buflen = (mxGetM(array_ptr) * mxGetN(array_ptr)) + 1; 
	   status = mxGetString(array_ptr, charbuf, buflen);  
	   if (status == 0)
	     mexPrintf("The converted string is \n%s.\n", charbuf);
	   else
	     mexErrMsgTxt("Could not convert string data.");
       
	   strcpy(tsInfo[timeseries].pakfile, charbuf);
	 }
       /*
       mexPrintf("Pakfile = %s\n", tsInfo[timeseries].pakfile);
       */
       /* Copy the "geomfile" string from prhs[2] and place it into charbuf.*/ 
       array_ptr = mxGetField(prhs[2],timeseries,field_names[7]);
       if(array_ptr == NULL)
	 {
	   buflen=1;
	   strcpy(tsInfo[timeseries].geomfile, "\0");
	 }
       else
	 {
	   buflen = (mxGetM(array_ptr) * mxGetN(array_ptr)) + 1; 
	   status = mxGetString(array_ptr, charbuf, buflen);  
	   if (status == 0)
	     mexPrintf("The converted string is \n%s.\n", charbuf);
	   else
	     mexErrMsgTxt("Could not convert string data.");
       
	   strcpy(tsInfo[timeseries].geomfile, charbuf);
	 }
       /*
       mexPrintf("Geomfile = %s\n", tsInfo[timeseries].geomfile);
       */

       /* Copy the "label" string from prhs[2] and place it into charbuf.*/ 

       array_ptr = mxGetField(prhs[2],timeseries,field_names[8]);
       if(array_ptr == NULL)
	 {
	   buflen=1;
	   strcpy(tsInfo[timeseries].label, "\0");
	 }
       else
	 {
	   buflen = (mxGetM(array_ptr) * mxGetN(array_ptr)) + 1; 
	   status = mxGetString(array_ptr, charbuf, buflen);  
	   if (status == 0)
	     mexPrintf("The converted string is \n%s.\n", charbuf);
	   else
	     mexErrMsgTxt("Could not convert string data.");
       
	   strcpy(tsInfo[timeseries].label, charbuf);
	 }
       /*
       mexPrintf("Label = %s\n", tsInfo[timeseries].label);
       */
       /* Get Pointer for potvals */
       array_ptr = mxGetField(prhs[2],timeseries,field_names[9]);       
       pr = mxGetPr(array_ptr);

       tsInfo[timeseries].potvals = (float **) 
	 calloc((size_t)tsInfo[timeseries].numframes, sizeof(float *));

       for (framenum=0; framenum<tsInfo[timeseries].numframes; framenum++)
	 tsInfo[timeseries].potvals[framenum] = (float *) 
	   calloc((size_t)tsInfo[timeseries].numleads, sizeof(float));
    
	 for (framenum=0, potind=0; framenum<tsInfo[timeseries].numframes; framenum++)
	   for (leadnum=0; leadnum<tsInfo[timeseries].numleads; leadnum++, potind++)
	     tsInfo[timeseries].potvals[framenum][leadnum] = (float)pr[potind];      

     }

   SetTSinfo(buf, fileInfo, tsInfo); 
   
   free(tsInfo);
   
}



