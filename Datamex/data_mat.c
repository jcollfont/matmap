#include <stdio.h>
#include <stdlib.h>
#include "data_mat.h"
#include "graphicsio.h"

/* TSinfo *GetTSinfo(char *dataFileName, int begIndex, int endIndex)  */
TSinfo *GetTSinfo(char *dataFileName, int *timeseriesarray, int numTS) 
{
  TSinfo *tsInfo;

  FileInfoPtr fip;
  long fileType;
  long numSurface;
  long numBSurface;
  long numTSeries;
  CBoolean pSetting;
  long tsIndex, i=0;
  long framenum, leadnum, potind;
 /* int begIndex, endIndex;*/

  float *databuff; /* a temporal buffer for storing potvals */

  /*  mexPrintf("openfile_"); */
  if ( openfile_(dataFileName, 1, &fip) ) {
    mexErrMsgTxt("_openfile");
  }
  
  /*  mexPrintf("getfileinfo_"); */
  if ( getfileinfo_(fip, &fileType, &numSurface, &numBSurface,
		    &numTSeries, &pSetting) ) {
    mexErrMsgTxt("getfileinfo_");
  }
  
/*   if (numTSeries < (endIndex-begIndex+1)) { */
/*     mexErrMsgTxt("Not enough time series in this data file as you requested"); */
/*   } */

  if (numTSeries < timeseriesarray[numTS-1]) {
    mexErrMsgTxt("Not enough time series in this data file as you requested");
  }
  
  numTSeries = numTS; 

  tsInfo = (TSinfo *) calloc((size_t)numTS, sizeof(TSinfo));
  i =0;

  for (tsIndex = 0; tsIndex < numTS; tsIndex++, i++) {

    /*    mexPrintf("settimeseriesindex_");*/
    if ( settimeseriesindex_(fip, timeseriesarray[tsIndex]) < 0) {
      mexErrMsgTxt("settimeseriesindex_");
    }


    /*    mexPrintf("gettimeseriesfile_"); */
    if ( gettimeseriesfile_(fip, tsInfo[i].pakfile) )
      mexErrMsgTxt("settimeseriesfile_");
      
    /*
    if ( gettimeseriesgeomfile_(fip, tsInfo[i].geomfile) ) 
      mexErrMsgTxt("settimeseriesindex_");
      */

    /*    mexPrintf("gettimeseriesspecs_"); */
    if ( gettimeseriesspecs_(fip, &tsInfo[i].numleads, &tsInfo[i].numframes) )
       mexErrMsgTxt("gettimeseriesspecs_");
      
    /*    mexPrintf("gettimeserieslabel_"); */
    if ( gettimeserieslabel_(fip, tsInfo[i].label) ) 
      mexErrMsgTxt("gettimeserieslabel_");
      
    /*    mexPrintf("gettimeseriesformat_"); */
    if ( gettimeseriesformat_(fip, &tsInfo[i].format) ) 
       mexErrMsgTxt("gettimeseriesformat_");
      
    /*    mexPrintf("gettimeseriesunits_"); */
    if ( gettimeseriesunits_(fip, &tsInfo[i].units) ) 
       mexErrMsgTxt("gettimeseriesunits_");
      
    
    databuff = (float *) calloc((size_t)(tsInfo[i].numframes * 
					 tsInfo[i].numleads),
				sizeof(float));
    
    /*    mexPrintf("gettimeseriesdata_"); */
    if ( gettimeseriesdata_(fip, databuff) ) {
      mexErrMsgTxt("gettimeseriesdata_");
     
    }
    tsInfo[i].potvals = (float **) calloc((size_t)tsInfo[i].numframes, 
					  sizeof(float *));

    for (framenum=0; framenum<tsInfo[i].numframes; framenum++)
      tsInfo[i].potvals[framenum] = (float *) 
	calloc((size_t)tsInfo[i].numleads, sizeof(float));
    
    for (framenum=0, potind=0; framenum<tsInfo[i].numframes; framenum++)
      for (leadnum=0; leadnum<tsInfo[i].numleads; leadnum++, potind++) 
	tsInfo[i].potvals[framenum][leadnum] = databuff[potind];
    free(databuff);
    
    /*    mexPrintf("getnumcorrectedleads_"); */
    if ( getnumcorrectedleads_(fip, &tsInfo[i].numbadleads) ) {
      mexErrMsgTxt("getnumcorrectedleads_");
      
    }
    
    tsInfo[i].badleadlist = (long *)
      calloc((size_t)tsInfo[i].numbadleads, sizeof(long));
    
    /*    mexPrintf("getcorrectedleads_"); */
    if ( getcorrectedleads_(fip, tsInfo[i].badleadlist) )
      mexErrMsgTxt("getcorrectedleads_");
      
  }
  
  closefile_( fip );
  return tsInfo;
}

int SetTSinfo(char *dataFileName, FInfo *fileInfo, TSinfo *tsInfo)
{
  FileInfoPtr fip;
  long i=0;
  long framenum, leadnum;

  float *databuff;          /* a temparol buffer for storing data */    
  long potind = 0;

  /*  if ( createfile_(dataFileName, 1, 1, &fip) ) {*/
  /*  mexPrintf("createfile_"); */
  if ( createfile_(dataFileName, 0, 1, &fip) ) {
    mexErrMsgTxt("createfile_");
  }
  
  /*  mexPrintf("setexpid_"); */
  if ( setexpid_(fip, fileInfo->expid) < 0) {
      mexErrMsgTxt("setexpid_");      
    }

  /*  mexPrintf("settext_"); */
  if ( settext_(fip, fileInfo->text) < 0) {
      mexErrMsgTxt("settext_");
  }
  
  for (i = 0; i < fileInfo->numts; i ++) {
    if ( settimeseriesindex_(fip, i+1) < 0) {
	mexErrMsgTxt("settimeseriesindex_");
	
    }
    if ( settimeseriesfile_(fip, tsInfo[i].pakfile) )
	mexErrMsgTxt("settimeseriesindex_");
    /*
    if ( settimeseriesgeomfile_(fip, tsInfo[i].geomfile) ) 
       mexErrMsgTxt("settimeseriesindex_");
       */
    if ( settimeseriesspecs_(fip, tsInfo[i].numleads, tsInfo[i].numframes) ) 
       mexErrMsgTxt("settimeseriesindex_");
    if ( settimeserieslabel_(fip, tsInfo[i].label) ) 
       mexErrMsgTxt("settimeseriesindex_");
    if ( settimeseriesformat_(fip, tsInfo[i].format) ) 
       mexErrMsgTxt("settimeseriesindex_");
    if ( settimeseriesunits_(fip, tsInfo[i].units) ) 
       mexErrMsgTxt("settimeseriesindex_");
    
     if (tsInfo[i].potvals == NULL) {
       mexErrMsgTxt("Dat buffer is empty, I can't let you continue!");
       
     }
     databuff = (float *)
       mxCalloc((size_t)(tsInfo[i].numframes*tsInfo[i].numleads),sizeof(float));
     potind = 0;
     for (framenum=0; framenum<tsInfo[i].numframes; framenum++)
       for (leadnum=0; leadnum<tsInfo[i].numleads; leadnum++, potind++) 
	 databuff[potind] = tsInfo[i].potvals[framenum][leadnum];
     if ( settimeseriesdata_(fip, databuff) ) 
        mexErrMsgTxt("settimeseriesdata_");

     free(databuff);

     if (tsInfo[i].numbadleads > 0) {
       if ( setnumcorrectedleads_(fip, tsInfo[i].numbadleads) )
	  mexErrMsgTxt("settimeseriesindex_");
       if ( setcorrectedleads_(fip, tsInfo[i].badleadlist) )
	  mexErrMsgTxt("settimeseriesindex_");
    }
  }
  
  closefile_(fip);
  return 0;
}   

FInfo *GetFileInfo(char *dataFileName)
{
  FileInfoPtr fip;
  long fileType;
  long numSurface;
  long numBSurface;
  long numTSeries;
  CBoolean pSetting;
  FInfo *fileInfo;

  /*  mexPrintf("_openfile"); */
  if ( openfile_(dataFileName, 1, &fip) )
    mexErrMsgTxt("_openfile");
  
  fileInfo = (FInfo *) calloc((size_t)1, sizeof(FInfo));
  
  /*  mexPrintf("_getfileinfo"); */
  if ( getfileinfo_(fip, &fileType, &numSurface, &numBSurface,
		    &numTSeries, &pSetting) )
      mexErrMsgTxt("_getfileinfo");

  fileInfo->numts = numTSeries;

  /*  mexPrintf("_gettext"); */
  if ( gettext_(fip, fileInfo->text) )
      mexErrMsgTxt("_gettext");

  /*  mexPrintf("_getexpid"); */
  if ( getexpid_(fip, fileInfo->expid) )
      mexErrMsgTxt("_getexpid");
  closefile_( fip );

  return fileInfo;
}
