C MEX-files for accessing CVRTI graphicsio files
# Last update: Mon Jan 22 21:43:40 2001 by Rob MacLeod
#    - Rob updates for Version 6 of Matlab.

------
Uasage
------
read_file
	    fileInfo = read_file(dataFileName);
	fileInfo is a structure that will store file header information
	in the input dataFileName, eg.,

	fileInfo =
	
	     text: 'Some plaque data for Dana -- made by showphi'
	    expid: []
	    numts: 1

read_data
	    tsinfo = read_data(dataFileName, begTS, endTS);
	tsinfo is an array of structure that will store information 
	from time series #begTS (starting 1) to #endTS of the input 
	dataFileName. begTS and endTS are optional, if omitted, will 
	take all the time series in the input file. eg.,

	tsinfo = 

	        numleads: 256
	       numframes: 549
		  format: 0
		   units: 2
	     numbadleads: 0
	badleadlistarray: []
	         pakfile: []
	        geomfile: []
		   label: ''
	          potval: [256x549 double]

write_data
	    write_data(dataFileName, fileInfo, tsInfo)
	fileInfo and tsInfo are structures like the output from 
	read_file and read_data. 

-------
Compile
-------
	make appropriate changes in mexopts.sh then run 
	mex read_file.c
	mex read_data.c
	mex write_data.c

     or use the makefile

        make clean
	make

---- Note ---- 

You have to match the version of the mex files you make to the ABI of Irix.
Mathworks dos not support n32 (groan) and so the options are compiling it
in old 32 mode or the new 64-bit mode.  Then you have to be sure to link to
the right version of the graphicsio library and then run the correct
version of matlab.

To get .mexsg (the o32 version), compile them on an sgi with the o32 mode
turned on--this requires hacking the mexopts.sh file because it read the
value of ARCH from someplace and always assumes 64 bit if the machine can
handle it, which all but eli.cvrti.utah.edu do.  So look at ths mexopts.sh
file and comment in or out the lines you want in orer to make the o32 or 64
bit versions. 

As of 2001, we finally have an n32 version of Matlab and so can make a
sensible version of the mex files and link them against the libraries we
actually use.  So I (Rob) changed the mexopts.sh file accordingly and
things seem to work.