% this script is to demo how to write a data file

% first construct a fInfo structure;
fInfo.text = 'Some text in the file header';
fInfo.expid = 'BT6apr95';
fInfo.numts = 1;

% then a tsInfo structure;
tsInfo.numleads = 0;
tsInfo.numframes = 0;
tsInfo.format = 0;
tsInfo.units = 0;
tsInfo.numbadleads = 0;
tsInfo.badleadlistarray = [];
tsInfo.pakfile = '';
tsInfo.geomfile = '';
tsInfo.label = '';
tsInfo.potval = [];

% now stuff a sin and cos wave into the tsInfo structure
x = 1:100;    % get 100 frames
x = x/10;     % do some scaling;
y1 = sin(x);
y2 = cos(x);

tsInfo.numleads = 2;
tsInfo.numframes = 100;
tsInfo.format = 2;        % 1=muxdata, 2=scalars, 3=raw, 4=pak
tsInfo.units = 1;         % 1=mV, 2=uV, 3=msec, 4=V, 5=mv*msec
tsInfo.potval = [y1; y2]; % potval[numleads-by-numframes];

% write data
write_data('tmp.tsdf', fInfo, tsInfo);